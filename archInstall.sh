#!/bin/bash

echo "pruefe wget..."; command -v wget >/dev/null 2>&1 || { echo >&2 "wget ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }
echo "pruefe parted..."; command -v parted >/dev/null 2>&1 || { echo >&2 "parted ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }
echo "pruefe sha2sum..."; command -v sha1sum >/dev/null 2>&1 || { echo >&2 "sha1sum ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }

echo "Name des zu erstellenden Benutzers eingeben:"
read USER

echo "Soll eine grafische Oberflaeche installiert werden?(J/N) "
read XORG
if [ $XORG == "J" -o $XORG == "j" ]
then
  echo "Welcher Desktop soll installiert werden? (xfce | kde )"
  read DESKTOP

  echo "Soll eine minimale Installation der Oberflaeche vorgenommen werden? ( J/N )"
  read MINIMAL
fi

echo "wlan? (J / N)"
read WIFI

echo "Soll die empfohlene Partitionierung verwendet werden? ( J | N )"
echo "Herbei werden drei Patirionen (boot[500MB], swap [4GB] und root[rest]) angelegt"
echo "----------------------------------------------------------------"
echo "!!! HINWEIS: alle daten werden dabei gelöscht !!!"
echo "----------------------------------------------------------------"
read PART_DEFAULT

echo "Wie Groß soll der Swap werden(in MiB)?"
read SWAPSIZE

if [ $PART_DEFAULT == "J" -o $PART_DEFAULT == "j" ]; then

  echo "Warte 10 Sekunden. Letzte Gelgenheit mit Strg + c abzubrechen..."
  sleep 10
  echo "Partitioniere..."

  BIOSSTART=0
  BIOSEND=$(($BIOSSTART+2))
  BOOTEND=$(($BIOSEND+128))
  SWAPEND=$(($BOOTEND+SWAPSIZE))
  EINHEIT=MiB

  parted /dev/sda mklabel gpt --script
  parted /dev/sda mkpart primary $BIOSSTART $BIOSEND$EINHEIT --align=min --script
  parted /dev/sda mkpart primary $BIOSEND$EINHEIT $BOOTEND$EINHEIT --align=min --script
  parted /dev/sda mkpart primary $BOOTEND$EINHEIT $SWAPEND$EINHEIT --align=min --script
  parted /dev/sda mkpart primary $SWAPEND$EINHEIT 100% --align=min --script

  ROOTFS=sda4
  BOOTFS=sda2
  SWAPFS=sda3
else
  echo "Partitionieren der Festplatte."
  echo "Soll die Festplatte neu Partitioniert werden? ( J | N )"
  echo "Stellen Sie sicher, dass drei Partitionen existieren, wenn sie dies nicht tun moechten!!!"
  read PART
  if [ $PART == "J" -o $PART == "j" ]
  then
    echo "entfernen der Partionstabelle"
    echo "Solle die Festplatte sicher bereinigt werden? ( J | N )"
    read WIPE
    echo "Warte 10 Sekunden. Letzte Gelgenheit mit Strg + c abzubrechen..."
    sleep 10

    if [ $WIPE == "j" -o $WIPE == "J" ]
    then
      dd if=/dev/zero of=/dev/sda status=progress
    else
      dd if=/dev/zero of=/dev/sda bs=512 count=1
    fi
    sleep 2
    echo "erstellen Sie eine boot(sda1), eine root(sda3) und eine swap(sda2) Partition."
    sleep 2

    cfdisk /dev/sda
  fi

  echo "Bitte Root-Partition eingeben (z.b. sda3): "
  read ROOTFS
  echo "Bitte Boot-Partition eingeben (z.b. sda1): "
  read BOOTFS
  echo "Bitte Swap-Partition eingeben (z.b. sda2): "
  read SWAPFS

fi

if [ $PART_DEFAULT == "j" -o $PART_DEFAULT == "J" ]; then
  echo "Formatiere..."
  parted /dev/sda set 1 bios_grub on --script
  parted /dev/sda set 2 boot on --script
  mkfs.ext4 /dev/$ROOTFS
  mkfs.ext2 /dev/$BOOTFS
  mkswap /dev/$SWAPFS
  mkfs.vfat /dev/sda1
else
  echo "Sollen die Partitionen neu formatiert werden(empfohlen)? ( J | N )"
  read FORMAT
  if [ $FORMAT == "j" -o $FORMAT == "J" ]
  then
    echo "Formatiere..."
    mkfs.ext4 /dev/$ROOTFS
    mkfs.ext2 /dev/$BOOTFS
    mkswap /dev/$SWAPFS
    mkfs.vfat /dev/sda2
  fi
fi

mkdir arch
#mv *.tar.gz ./arch

cd ./arch


wget -r -nd --no-parent -e robots=off -A '*.tar.gz' https://mirrors.edge.kernel.org/archlinux/iso/latest/
wget -r -nd --no-parent -e robots=off -A 'sha*.txt' https://mirrors.edge.kernel.org/archlinux/iso/latest/

cat sha1sums.txt | grep boots > bootstrapSum.txt
sha1sum -c bootstrapSum.txt
sleep 5
rm *.txt

tar xzf *.tar.gz
rm *.tar.gz

mount /dev/$ROOTFS ./root.x86_64/mnt
mkdir -p ./root.x86_64/mnt/boot
mount /dev/$BOOTFS ./root.x86_64/mnt/boot
rm -r ./root.x86_64/mnt/boot/*
swapon /dev/$SWAPFS

mount --bind ./arch/root.x86_64 ./arch/root.x86_64
cd ./root.x86_64
cp /etc/resolv.conf etc
mount -t proc /proc proc
mount --make-rslave --rbind /sys sys
mount --make-rslave --rbind /dev dev
mount --make-rslave --rbind /run run    # (assuming /run exists on the system)

nano ./etc/pacman.d/mirrorlist

echo "#!/bin/bash

USER=$USER
XORG=$XORG
DESKTOP=$DESKTOP
WIFI=$WIFI
MINIMAL=$MINIMAL

source /etc/profile

pacman-key --init
pacman-key --populate archlinux

pacman -Syy
pacman -S --noconfirm base

pacstrap /mnt base

genfstab -U /mnt >> /mnt/etc/fstab

clear
echo \"------------Beginne arch-chroot-------------------\"
echo \"Fuehren Sie \\\"./setupArch && exit\\\" aus.\"
arch-chroot /mnt

exit
" > installerArch

chmod +x installerArch

echo "#!/bin/bash

USER=$USER
XORG=$XORG
DESKTOP=$DESKTOP
WIFI=$WIFI
MINIMAL=$MINIMAL

echo \"de_DE.UTF-8\" >> /etc/locale.gen
locale-gen
echo \"LANG=de_DE.UTF-8\" > /etc/locale.conf

export LANG=de_DE.UTF-8
echo \"KEYMAP=de-latin1-nodeadkeys\" > /etc/vconsole.conf
localectl set-x11-keymap de-latin1-nodeadkeys
ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc --utc

echo \"arch\" > /etc/hostname

mkdir -p installState

if [ ! -e installState/02network ]
then
  clear
  echo \"-----------Netzwerktools---------------\"
  systemctl enable dhcpcd@enp0s3.service
  if [ \"\$WIFI\" == \"J\" -o \"\$WIFI\" == \"j\" ]
  then
    pacman -S --noconfirm iw wpa_supplicant
    pacman -S --noconfirm dialog

    wifi-menu

    pacman -S --noconfirm wpa_actiond
  fi
  touch installState/02network
fi

mkinitcpio -p linux

if [ ! -e installState/03users ]
then
  clear
  echo \"root Passwort setzen:\"
  passwd

  clear
  echo \"Der Benutzer \$USER wird angelegt\"
  useradd -g users -G wheel,audio,video -m \$USER
  passwd \$USER
  touch installState/03users
fi

if [ ! -e installState/07bootloader ]
  then
    clear
    echo \"Bootloader installieren\"
    pacman -S --noconfirm grub
    grub-mkconfig -o /boot/grub/grub.cfg
    grub-install /dev/sda
    touch installState/07bootloader
fi

if [ ! -e installState/09gui ]
  then
  clear

  if [ \"\$XORG\" == \"J\" -o \"\$XORG\" == \"j\" ]
  then
    pacman -S --noconfirm xorg
		pacman -S --noconfirm xorg-xinit
		pacman -S --noconfirm xterm xorg-twm

    case \"\$DESKTOP\" in
      xfce) if [ \"\$MINIMAL\" == \"J\" -o \"\$MINIMAL\" == \"j\" ]
            then
              pacman -S --noconfirm xfce4
            else
              pacman -S --noconfirm xfce4 xfce4-goodies
            fi
            ;;
      kde)  if [ \"\$MINIMAL\" == \"J\" -o \"\$MINIMAL\" == \"j\" ]
            then
              pacman -S --noconfirm plasma-desktop plasma-wayland-session
            else
              pacman -S --noconfirm plasma plasma-wayland-session
            fi
            ;;
    esac
  fi

  pacman -S --noconfirm sddm
  #echo \"setxkbmap \\\"de\\\"\" >> /usr/share/sddm/scripts/Xsetup
  mkdir -p /etc/sddm.conf.d
  sddm --example-config > /etc/sddm.conf.d/sddm.conf
  systemctl enable sddm.service
  touch installState/09gui
fi

exit
" > ./mnt/setupArch
chmod +x ./mnt/setupArch

clear
echo "----------Beginn Chroot--------------"
echo "Fuehren Sie \"./installerArch && exit\" aus."
chroot . /bin/bash

sync && umount -R ./mnt
swapoff /dev/$SWAPFS
