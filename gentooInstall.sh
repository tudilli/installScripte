#!/bin/bash

echo "pruefe links..."; command -v links >/dev/null 2>&1 || { echo >&2 "Links-Browser ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }
echo "pruefe parted..."; command -v parted >/dev/null 2>&1 || { echo >&2 "parted ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }
echo "pruefe cfdisk..."; command -v cfdisk >/dev/null 2>&1 || { echo >&2 "cfdisk ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }
echo "pruefe nano..."; command -v nano >/dev/null 2>&1 || { echo >&2 "nano ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }
echo "pruefe mkfs.fat..."; command -v mkfs.fat >/dev/null 2>&1 || { echo >&2 "nano ist nicht installiert. Bitte installieren und erneut ausfuehren"; exit 1; }

echo "Name des zu erstellenden Benutzers eingeben:"
read USER

echo "Soll Gen-Kernel verwendet werden? ( J/N )"
echo "Ohne Gen-Kernel muss der Kernel von Hand konfiguriert werden. Aber das Kompilieren des Gen-Kernel dauert wesendlich laenger."
read GENKERNEL

echo "Sollen Linzensen automatisch akzeptiert werden? ( J/N )"
read LICENZ

echo "Soll eine grafische Oberflaeche installiert werden?( J/N ) "
read XORG
if [ $XORG == "J" -o $XORG == "j" ]
then
  echo "Welcher Desktop soll installiert werden? (xfce | mate | kde | openbox | i3 | jwm | blackbox | awesome | dwm)"
  read DESKTOP

  echo "Soll eine minimale Installation vorgenommen werden? ( J/N )"
  read MINIMAL

  echo "Soll eine grafischer Loginmanager verwendet werden?( J/N ) "
  read LOGIN

fi
echo "wlan? ( J/N )"
read WIFI

echo "Moechten Sie binaere Pakete verwenden falls vorhanden? ( J/N )"
read BINDIST

echo "Moechten Sie die Passwortrichtlinien bearbeiten? ( J/N )"
read PASS

echo "Moechten Sie git zur Repository-Synchronisation nutzen? ( J/N )"
read GIT

echo "Sollen zusaetzliche Programme installiert werden? ( J/N )"
read ZUSATZ

if [ $ZUSATZ == "j" -o $ZUSATZ == "J" ]
  then
    ZUSATZPAKETE="www-client/links"

    AUSWAHL="n"
    echo "sudo? ( J/N )"
    read AUSWAHL
    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        ZUSATZPAKETE="$ZUSATZPAKETE app-admin/sudo"
    fi

    echo "Sollen Webbrowser installiert werden? ( J/N )"
    read AUSWAHL

    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        AUSWAHL="n"
        echo "Firefox? ( J/N )"
        read AUSWAHL
        if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
          then
            if [ $BINDIST == "j" -o $BINDIST == "J" ]
              then
                ZUSATZPAKETE="$ZUSATZPAKETE www-client/firefox-bin"
              else
                ZUSATZPAKETE="$ZUSATZPAKETE www-client/firefox"
            fi
        fi

        AUSWAHL="n"
        echo "Chromium? ( J/N )"
        read AUSWAHL
        if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
          then
            ZUSATZPAKETE="$ZUSATZPAKETE www-client/chromium"
        fi

        AUSWAHL="n"
        echo "Midori? ( J/N )"
        read AUSWAHL
        if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
          then
            ZUSATZPAKETE="$ZUSATZPAKETE www-client/midori"
        fi

        AUSWAHL="n"
        echo "qutebrowser? ( J/N )"
        read AUSWAHL
        if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
          then
            ZUSATZPAKETE="$ZUSATZPAKETE www-client/qutebrowser"
        fi
    fi

    AUSWAHL="n"
    echo "Sublime-Text Editor? ( J/N )"
    read AUSWAHL
    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        ZUSATZPAKETE="$ZUSATZPAKETE app-editors/sublime-text"
    fi

    AUSWAHL="n"
    echo "git? ( J/N )"
    read AUSWAHL
    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        ZUSATZPAKETE="$ZUSATZPAKETE dev-vcs/git"
    fi

    AUSWAHL="n"
    echo "openjdk? ( J/N )"
    read AUSWAHL
    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        if [ $BINDIST == "j" -o $BINDIST == "J" ]
          then
            ZUSATZPAKETE="$ZUSATZPAKETE dev-java/openjdk-bin"
          else
            ZUSATZPAKETE="$ZUSATZPAKETE dev-java/openjdk"
        fi
    fi

    AUSWAHL="n"
    echo "Libreoffice? ( J/N )"
    read AUSWAHL
    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        if [ $BINDIST == "j" -o $BINDIST == "J" ]
          then
            ZUSATZPAKETE="$ZUSATZPAKETE app-office/libreoffice-bin"
          else
            ZUSATZPAKETE="$ZUSATZPAKETE app-office/libreoffice"
        fi
        ZUSATZPAKETE="$ZUSATZPAKETE app-office/libreoffice-l10n"
    fi

    AUSWAHL="n"
    echo "VirtualBox-Guest-Additions? ( J/N )"
    read AUSWAHL
    if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
      then
        ZUSATZPAKETE="$ZUSATZPAKETE app-emulation/virtualbox-guest-additions"
    fi

  echo "Sollen Emulatoren fuer Retrokonsolen installiert werden?( J/N ) "
  read EMU
  if [ $EMU == "J" -o $EMU == "j" ]
  then
    echo "Sollen alle empfohlenen Emulatoren werden?( J/N ) "
    echo "Diese Auswahl beinhaltet die folgenden Systeme: "
    echo "   games-emulation/desmune    NDS "
    echo "   games-emulation/dolphine   GC Wii "
    echo "   games-emulation/nestopia   NES "
    echo "   games-emulation/pcsx2      PS2 "
    echo "   games-emulation/pcsxr      PS1 "
    echo "   games-emulation/ppsspp     PSP "
    echo "   games-emulation/snes9x     SNES "
    echo "   games-emulation/vbam       GB GBC GBA "
    read EMUFULL
    if [ $EMUFULL == "J" -o $EMUFULL == "j" ]
    then
      ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/desmume games-emulation/dolphine games-emulation/nestopia games-emulation/pcsx2 games-emulation/pcsxr games-emulation/ppsspp games-emulation/snes9x games-emulation/vbam"
    else
      AUSWAHL="n"
      echo "games-emulation/desmune    NDS ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/desmume"
      fi
      AUSWAHL="n"
      echo "games-emulation/dolphine    GC Wii ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/dolphine"
      fi
      AUSWAHL="n"
      echo "games-emulation/nestopia   NES ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/nestopia"
      fi
      AUSWAHL="n"
      echo "games-emulation/pcsx2      PS2 ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/pcsx2"
      fi
      AUSWAHL="n"
      echo "games-emulation/pcsxr      PS1 ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/pcsxr"
      fi
      AUSWAHL="n"
      echo "games-emulation/ppsspp     PSP ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/ppsspp"
      fi
      AUSWAHL="n"
      echo "games-emulation/snes9x     SNES ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/snes9x"
      fi
      AUSWAHL="n"
      echo "games-emulation/vbam       GB GBC GBA ( J/N )"
      read AUSWAHL
      if [ $AUSWAHL == "j" -o $AUSWAHL == "J" ]
        then
          ZUSATZPAKETE="$ZUSATZPAKETE games-emulation/vbam"
      fi
    fi
  fi
fi


echo "Soll Festplattenspeicher-Optimierung verwendet werden? ( J/N )"
read OPTIMIZE

echo "Wieviele Prozesse sollen zum Uebersetzen verwendet werden?"
echo "Hier ist eine gute Wahl die Anzahl der Prozessorkerne zu verwenden."
read THREADS

echo "Soll EFI Boot verwendet werden? ( J/N )"
read EFI

echo "Soll die empfohlene Partitionierung verwendet werden? ( J | N )"
echo "Hierbei werden drei Partitionen (boot[500MB], swap [Nach Wahl] und root[rest]) angelegt"
echo "----------------------------------------------------------------"
echo "!!! HINWEIS: alle daten werden dabei geloescht !!!"
echo "----------------------------------------------------------------"
read PART_DEFAULT

#echo "Soll eine seperate Home-Partition angelegt werden?"
#read HOMEPART

# if [ $HOMEPART == "J" -o $HOMEPART == "j" ]; then
#   echo "Wie gross soll die Home-Partition werden(in MiB)?"
#   read HOMESIZE
# fi

echo "Wie gross soll der Swap werden(in MiB)?"
read SWAPSIZE


if [ $PART_DEFAULT == "J" -o $PART_DEFAULT == "j" ]; then

  echo "Warte 10 Sekunden. Letzte Gelgenheit mit Strg + c abzubrechen..."
  sleep 10
  echo "Partitioniere..."

  BIOSSTART=0
  BIOSEND=$(($BIOSSTART+2))
  BOOTEND=$(($BIOSEND+256))
  SWAPEND=$(($BOOTEND+SWAPSIZE))
  EINHEIT=MiB

  parted /dev/sda mklabel gpt --script
  parted /dev/sda mkpart primary $BIOSSTART $BIOSEND$EINHEIT --align=min --script
  parted /dev/sda mkpart primary $BIOSEND$EINHEIT $BOOTEND$EINHEIT --align=min --script
  parted /dev/sda mkpart primary $BOOTEND$EINHEIT $SWAPEND$EINHEIT --align=min --script
  parted /dev/sda mkpart primary $SWAPEND$EINHEIT 100% --align=min --script

  ROOTFS=sda4
  BOOTFS=sda2
  SWAPFS=sda3
else
  echo "Partitionieren der Festplatte."
  echo "Soll die Festplatte neu Partitioniert werden? ( J | N )"
  echo "Stellen Sie sicher, dass drei Partitionen existieren, wenn sie dies nicht tun moechten!!!"
  read PART
  if [ $PART == "J" -o $PART == "j" ]
  then
    echo "entfernen der Partionstabelle"
    echo "Solle die Festplatte sicher bereinigt werden? ( J | N )"
    read WIPE
    echo "Warte 10 Sekunden. Letzte Gelgenheit mit Strg + c abzubrechen..."
    sleep 10

    if [ $WIPE == "j" -o $WIPE == "J" ]
    then
      dd if=/dev/zero of=/dev/sda status=progress
    else
      dd if=/dev/zero of=/dev/sda bs=512 count=1
    fi
    sleep 2
    echo "erstellen Sie eine boot(sda1), eine root(sda3) und eine swap(sda2) Partition."
    sleep 2

    cfdisk /dev/sda
  fi

  echo "Bitte Root-Partition eingeben (z.b. sda3): "
  read ROOTFS
  echo "Bitte Boot-Partition eingeben (z.b. sda1): "
  read BOOTFS
  echo "Bitte Swap-Partition eingeben (z.b. sda2): "
  read SWAPFS

fi

if [ $PART_DEFAULT == "j" -o $PART_DEFAULT == "J" ]; then
  echo "Formatiere..."
  parted /dev/sda set 1 bios_grub on --script
  parted /dev/sda set 2 boot on --script
  mkfs.ext4 /dev/$ROOTFS
  if [ $EFI == "j" -o $EFI == "J" ]; then
    parted /dev/sda set 2 esp on --script
    mkfs.fat -F 32 /dev/$BOOTFS
  else
    mkfs.ext2 /dev/$BOOTFS
  fi
  mkswap /dev/$SWAPFS
  mkfs.vfat /dev/sda1
else
  echo "Sollen die Partitionen neu formatiert werden(empfohlen)? ( J | N )"
  read FORMAT
  if [ $FORMAT == "j" -o $FORMAT == "J" ]
  then
    echo "Formatiere..."
    mkfs.ext4 /dev/$ROOTFS
    if [ $EFI == "j" -o $EFI == "J" ]; then
      mkfs.fat -F 32 /dev/$BOOTFS
    else
      mkfs.ext2 /dev/$BOOTFS
    fi
    mkswap /dev/$SWAPFS
    mkfs.vfat /dev/sda1
  fi
fi

mkdir -p /mnt/gentoo
mount /dev/$ROOTFS /mnt/gentoo
mkdir -p /mnt/gentoo/boot
mount /dev/$BOOTFS /mnt/gentoo/boot
swapon /dev/$SWAPFS

cd /mnt/gentoo

if [ ! -f stage3* ]
then
  clear
  echo "--------stage3 herunterladen---------"
  sleep 3

  links www.gentoo.org
fi

echo "entpacke stage3..."
tar xpf stage3* --xattrs-include='*.*' --numeric-owner

if [ $BINDIST == "j" -o $BINDIST == "J" ]; then
  echo "USE=\"bindist\"" > /mnt/gentoo/etc/portage/make.conf
else
  echo "USE=\"-bindist\"" > /mnt/gentoo/etc/portage/make.conf
fi

echo "LINGUAS=\"de en en_US\"" >> /mnt/gentoo/etc/portage/make.conf
echo "L10N=\"de en en_US\"" >> /mnt/gentoo/etc/portage/make.conf
if [ $OPTIMIZE == "j" -o $OPTIMIZE == "J" ]; then
  echo "COMMON_FLAGS=\"-march=native -Os -pipe\"" >> /mnt/gentoo/etc/portage/make.conf
else
  echo "COMMON_FLAGS=\"-march=native -O2 -pipe\"" >> /mnt/gentoo/etc/portage/make.conf
fi
echo "CFLAGS=\"\$COMMON_FLAGS\"" >> /mnt/gentoo/etc/portage/make.conf
echo "CXXFLAGS=\"\$COMMON_FLAGS\"" >> /mnt/gentoo/etc/portage/make.conf

echo "MAKEOPTS=\"-j$THREADS\"" >> /mnt/gentoo/etc/portage/make.conf

if [ $LICENZ == "j" -o $LICENZ == "J" ]; then
  echo "ACCEPT_LICENSE=\"*\"" >> /mnt/gentoo/etc/portage/make.conf
fi

echo --make.conf ueberpruefen--
sleep 5
nano /mnt/gentoo/etc/portage/make.conf

mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
mount --bind /run /mnt/gentoo/run
mount --make-slave /mnt/gentoo/run 

cp --dereference /etc/resolv.conf etc

mkdir --parents /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

clear
echo "----------Beginn Chroot--------------"

echo "#!/bin/bash

source /etc/profile

USER=$USER
LICENZ=$LICENZ
XORG=$XORG
DESKTOP=$DESKTOP
WIFI=$WIFI
MINIMAL=$MINIMAL
PASS=$PASS
LOGIN=$LOGIN
GENKERNEL=$GENKERNEL
THREADS=$THREADS
EFI=$EFI
ZUSATZ=$ZUSATZ
ZUSATZPAKETE=\"$ZUSATZPAKETE\"

mkdir -p installState

if [ ! -e installState/01portage ]
then
  touch installState/01portage.installing
  echo \"synkronisiere portage\"
  mkdir -p /usr/portage
  emerge-webrsync
  emerge --sync --quiet

  if [ $GIT == "J" -o $GIT == "j" ]
  then
    emerge -q app-eselect/eselect-repository dev-vcs/git
    eselect repository remove gentoo
    eselect repository add gentoo git https://github.com/gentoo-mirror/gentoo.git
    rm -r /var/db/repos/gentoo
    emaint sync -r gentoo
  fi
  
  echo \"done\"
  mv installState/01portage.installing installState/01portage
  sleep 2
fi

if [ ! -e installState/02users ]
then
  touch installState/02users.installing
  if [ ! -e installState/02users ]
  then
    nano /etc/security/passwdqc.conf
  fi

  clear
  echo \"root Passwort setzen:\"
  passwd

  clear
  echo \"Der Benutzer \$USER wird angelegt\"
  useradd -g users -G wheel,portage,audio,video,usb,cdrom -m \$USER
  passwd \$USER
  mv installState/02users.installing installState/02users
fi

if [ ! -e installState/03fstab ]
then
  touch installState/03fstab.installing
  clear
  echo \"schreibe fstab\"
  echo \"/dev/$ROOTFS      /          ext4     defaults,noatime     0 1\" > /etc/fstab
  if [ \"\$EFI\" == \"n\" -o \"\$EFI\" == \"N\" ]
  then
    echo \"/dev/$BOOTFS      /boot      ext2     defaults,noatime     0 2\" >> /etc/fstab
  else
    echo \"/dev/$BOOTFS      /boot      vfat     defaults,noatime     0 2\" >> /etc/fstab
  fi
  echo \"/dev/$SWAPFS      none       swap     sw                 0 0\" >> /etc/fstab
  echo \"/dev/cdrom        /mnt/cdrom auto     noauto,ro          0 0\" >> /etc/fstab
  echo \"none              /proc      proc     defaults           0 0\" >> /etc/fstab
  echo \"done\"
  mv installState/03fstab.installing installState/03fstab
  sleep 2
fi

if [ ! -e installState/04config ]
then
  touch installState/04config.installing
  clear
  echo \"----Konfiguriere System----\"
  echo \"de_DE.UTF-8 UTF-8\" > /etc/locale.gen
  echo \"en_US.UTF-8 UTF-8\" >> /etc/locale.gen

  locale-gen

  nano /etc/conf.d/keymaps
  nano /etc/conf.d/hostname
  echo \"\" > /etc/issue
  echo \"This is \\n (\\s \\m \\r) \\t\" >> /etc/issue

  ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
  mv installState/04config.installing installState/04config
fi

if [ ! -e installState/05profile ]
then
  touch installState/05profile.installing
  clear
  echo \"---------Profil auswaehlen---------\"

  echo \"Welche Profilstabilitaet soll installiert werden? ( exp | stable )\"
  read STABILITY

  if [ \"\$XORG\" == \"N\" -o \"\$XORG\" == \"n\" ]
  then
      eselect profile list | grep \$STABILITY
  fi

  if [ \"\$XORG\" == \"J\" -o \"\$XORG\" == \"j\" ]
  then
    case \"\$DESKTOP\" in
      mate) eselect profile list | grep \$STABILITY | grep desktop
            ;;
      kde)  eselect profile list | grep \$STABILITY | grep plasma
            ;;
      *)    eselect profile list | grep \$STABILITY
            ;;
    esac
  fi

  echo \"Nummer des Profils eingeben: \"
  read PROFILE
  eselect profile set \$PROFILE
  env-update && source /etc/profile

  emerge --quiet --noreplace --update --deep --newuse  --autounmask --autounmask-keep-masks --autounmask-write @world || etc-update --automode -5 && emerge --quiet --noreplace --update --deep --newuse  @world || exit 1
  echo \"Installiere Tools\"
  emerge -q app-portage/flaggie app-portage/gentoolkit app-admin/sudo

  mv installState/05profile.installing installState/05profile
fi

if [ ! -e installState/06kernel ]
then
  touch installState/06kernel.installing
  clear
  echo \"Kernel-quellen laden\"
  emerge --quiet --noreplace  --autounmask --autounmask-keep-masks --autounmask-write sys-kernel/gentoo-sources sys-kernel/linux-firmware || etc-update --automode -5 && emerge --quiet --noreplace sys-kernel/gentoo-sources sys-kernel/linux-firmware || exit 1
  ln -s /usr/src/linux* /usr/src/linux
  cd /usr/src/linux

  echo \"Baue Kernel\"

  if [ \"\$GENKERNEL\" == \"n\" -o \"\$GENKERNEL\" == \"N\" ]
  then
    clear
    make menuconfig
    make -j$THREADS
    make modules_install
    make install
  else
    clear
    emerge --quiet --noreplace --autounmask --autounmask-keep-masks --autounmask-write sys-kernel/genkernel || etc-update --automode -5 && emerge --quiet --noreplace sys-kernel/genkernel || exit 1
    genkernel all
  fi

  cd /
  mv installState/06kernel.installing installState/06kernel
fi

if [ ! -e installState/07bootloader ]
then
  touch installState/07bootloader.installing
  clear
  echo \"Bootloader installieren\"
  if [ \"\$EFI\" == \"n\" -o \"\$EFI\" == \"N\" ]
  then
    echo \"GRUB_PLATTFORMS=\\\"pc\\\"\" >> /etc/portage/make.conf
    emerge --quiet --noreplace sys-boot/grub
    grub-install /dev/sda
  else
    echo \"GRUB_PLATTFORMS=\\\"efi-64\\\"\" >> /etc/portage/make.conf
    emerge --quiet sys-boot/grub
    grub-install --target=x86_64-efi --efi-directory=/boot || mount -o remount,rw /sys/firmware/efi/efivars && grub-install --target=x86_64-efi --efi-directory=/boot || exit 1
  fi
  grub-mkconfig -o /boot/grub/grub.cfg
  mv installState/07bootloader.installing installState/07bootloader
fi

if [ ! -e installState/08network ]
then
  touch installState/08network.installing
  clear
  echo \"-----------Netzwerktools---------------\"
  PACKAGES=\"net-misc/netifrc sys-apps/iproute2 net-misc/dhcpcd\"
  if [ \"\$WIFI\" == \"J\" -o \"\$WIFI\" == \"j\" ]
  then
     PACKAGES=\"\$PACKAGES net-wireless/wireless-tools net-wireless/iw net-wireless/wpa_supplicant\"
     if [ \"\$XORG\" == \"J\" -o \"\$XORG\" == \"j\" ]
      then
        PACKAGES=\"\$PACKAGES net-misc/networkmanager\"
        flaggie net-wireless/wpa_subblicant +dbus
        flaggie net-misc/networkmanager +ncurses
      fi
  fi
  emerge --quiet --noreplace \$PACKAGES || exit 1

  if [ \"\$WIFI\" == \"J\" -o \"\$WIFI\" == \"j\" ]
  then
      echo \"# Prefer wpa_supplicant over wireless-tools\" > /etc/conf.d/net
      echo \"modules=\\\"wpa_subblicant\\\"\" >> /etc/conf.d/net
  fi

  mv installState/08network.installing installState/08network
fi

if [ ! -e installState/09gui ]
  then
  touch installState/09gui.installing
  clear

  if [ \"\$XORG\" == \"J\" -o \"\$XORG\" == \"j\" ]
  then
    if [ \"\$MINIMAL\" == \"J\" -o \"\$MINIMAL\" == \"j\" ]
      then
        PACKAGES=\"x11-base/xorg-server x11-terms/xterm\"
      else
        PACKAGES=\"x11-base/xorg-server x11-terms/xterm x11-base/xorg-apps x11-base/xorg-drivers x11-base/xorg-fonds\"
    fi

    case \"\$DESKTOP\" in
      xfce) if [ \"\$MINIMAL\" == \"J\" -o \"\$MINIMAL\" == \"j\" ]
            then
              echo \"xfce-base/xfce4-meta minimal\" >> /etc/portage/package.use/xfce4-meta.use
            fi
            PACKAGES=\"\$PACKAGES xfce-base/xfce4-meta xfce-extra/xfce4-notifyd xfce-extra/xfce4-volumed-pulse x11-terms/xfce4-terminal\"

            ;;
      kde)  echo \"dev-libs/openssl bindist\" >> /etc/portage/package.use/openssl.use
            echo \"dev-qt/qtnetwork bindist\" >> /etc/portage/package.use/qtnetwork.use

            if [ \"\$MINIMAL\" == \"J\" -o \"\$MINIMAL\" == \"j\" ]
            then
              PACKAGES=\"\$PACKAGES kde-plasma/plasma-desktop\"
            else
              PACKAGES=\"\$PACKAGES kde-plasma/plasma-meta\"
            fi
            ;;
      mate) PACKAGES=\"\$PACKAGES mate-base/mate\"
            if [ \"\$MINIMAL\" == \"N\" -o \"\$MINIMAL\" == \"n\" ]
            then
              PACKAGES=\"\$PACKAGES mate-extra/caja-extensions\"
            fi

            # if [ \"\$LOGIN\" == \"J\" -o \"\$LOGIN\" == \"j\" ]
            # then
            #   PACKAGE=\"\$PACKAGES x11-misc/lightdm\"
            # fi
            ;;
      openbox) PACKAGES=\"\$PACKAGES x11-wm/openbox\"
            if [ \"\$MINIMAL\" == \"N\" -o \"\$MINIMAL\" == \"n\" ]
            then
              PACKAGES=\"\$PACKAGES x11-misc/openbox-menu\"
            fi
            ;;
      i3) PACKAGES=\"\$PACKAGES x11-wm/i3\"
            if [ \"\$MINIMAL\" == \"N\" -o \"\$MINIMAL\" == \"n\" ]
            then
              PACKAGES=\"\$PACKAGES x11-misc/i3status x11-misc/i3lock x11-misc/dmenu\"
            fi
            ;;
      jwm) PACKAGES=\"\$PACKAGES x11-wm/jwm\"
            ;;
      blackbox) PACKAGES=\"\$PACKAGES x11-wm/blackbox\"
            ;;
      awesome) PACKAGES=\"\$PACKAGES x11-wm/awesome\"
            flaggie x11-wm/awesome +elogind
            ;;
      dwm) PACKAGES=\"\$PACKAGES x11-wm/dwm\"
            if [ \"\$MINIMAL\" == \"N\" -o \"\$MINIMAL\" == \"n\" ]
            then
              flaggie x11-wm/dwm +savedconfig +xinerama
              PACKAGES=\"\$PACKAGES x11-misc/dmenu x11-apps/xsetroot\"
            fi
            ;;
    esac

    if [ \"\$LOGIN\" == \"J\" -o \"\$LOGIN\" == \"j\" ]
    then
      #PACKAGES=\"\$PACKAGES x11-misc/lightdm\"
      #DM=\"lightdm\"
      if [ \"\$DESKTOP\" == \"KDE\" -o \"\$DESKTOP\" == \"kde\" ]
      then
        PACKAGES=\"\$PACKAGES x11-misc/sddm kde-plasma/sddm-kcm\"
      else
        PACKAGES=\"\$PACKAGES x11-misc/sddm\"
      fi
      DM=\"sddm\"
      usermod -a -G video sddm
    fi

  fi
  env-update && source /etc/profile

  clear

  echo \"-------installiere GUI--------\"
  emerge --quiet --noreplace --autounmask --autounmask-keep-masks --autounmask-write \$PACKAGES || etc-update --automode -5 && emerge --quiet --noreplace \$PACKAGES || exit 1

  if [ \"\$XORG\" == \"J\" -o \"\$XORG\" == \"j\" ]
  then
    if [ \$DESKTOP == \"xfce\" -o \$DESKTOP == \"mate\" ]
    then
      rc-update add dbus default
    fi

    if [ \"\$LOGIN\" == \"J\" -o \"\$LOGIN\" == \"j\" ]
    then
      echo \"DISPLAYMANAGER=\$DM \" >> /etc/conf.d/display-manager
      rc-update add xdm default
    fi
  fi

  mv installState/09gui.installing installState/09gui
fi

if [ ! -e installState/10installPackages ]
then
  touch installState/10installPackages.installing
  clear
  if [ \"\$ZUSATZ\" == \"J\" -o \"\$ZUSATZ\" == \"j\" ]
  then
    echo \"-------installiere Zusatzpakete--------\"
    emerge --quiet --noreplace --autounmask --autounmask-keep-masks --autounmask-write \$ZUSATZPAKETE || etc-update --automode -5 && emerge --quiet --noreplace \$ZUSATZPAKETE || exit 1
  fi
  mv installState/10installPackages.installing installState/10installPackages
fi

# rm /gentooSetup.sh

echo \"Synkronisiere Festplatte(n)\"
sync

if [ \"\$LOGIN\" == \"J\" -o \"\$LOGIN\" == \"j\" ]
    then
      echo \"Ueberpruefe die Konfiguration des Display-Managers\"
      echo \"Druecke ENTER um fortzufahren...\"
      read TEMP
      nano /etc/conf.d/xdm
    fi

echo \" Installation ist beendet. Tippen Sie exit um die Installationsumgebung zu verlassen.\"
echo \" Sie können an dieser stelle auch noch manuelle Anpassungen vornehmen.\"

" > gentooSetup.sh


chmod +x /mnt/gentoo/gentooSetup.sh

chroot . /bin/bash

echo "----------Ende Chroot-------------"
cd /mnt
sync
umount -R gentoo
swapoff /dev/$SWAPFS
echo "-----Fertig-----------------------"
